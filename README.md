# Pearcy 1997

Implementation of model published [here](https://doi.org/10.1046/j.1365-3040.1997.d01-88.x).

## Current status

**This model is not fully implemented**

## Setting up

- `pip install pre-commit`
- `pre-commit install`
- `pip install -r code/requirements.txt`
